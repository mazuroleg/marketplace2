import React from 'react';
import Main from '../componets/Main';
import itemDescriptions from '../screens/ItemDescripions';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
 const Stack = createStackNavigator();
 export default function Navigate() {
        return <NavigationContainer>
                  <Stack.Navigator>
                      <Stack.Screen
                      name='Main'
                      component={Main}
                      options={
                          {
                              title:'Головна',
                              headerStyle:{backgroundColor:"grey", height:50, },
                              headerBackTitleStyle:{fontWeight:"light", }
                    
                    }}
                      
                      />
                      <Stack.Screen
                      name='itemDescription'
                      component={itemDescriptions}
                      options={
                          {
                              title:'Опис',
                              headerStyle:{backgroundColor:"grey", height:50, },
                              headerBackTitleStyle:{fontWeight:"light", }
                    
                    }}
                      
                      />
                  </Stack.Navigator>


        </NavigationContainer>



 }