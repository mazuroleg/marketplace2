import React from 'react';
import {  Text, StyleSheet, View,Image,onPress } from 'react-native';
import { State, TouchableOpacity } from 'react-native-gesture-handler';



const ProductItem = ({ item,navigation }) =>  {
   
  return (
    
      <View style={styles.item}>
        
        <TouchableOpacity onPress={()=>navigation.navigate('itemDescription',item)}>
           <Image  source={{
                  width:'100%',
                  height:200,
                  uri:item.picture      

           }}/>
           <Text  style={styles.title}>{item.title}</Text>
            
            <Text  style={styles.description}>{item.description}</Text>
            <Text  style={styles.price}>${item.price}</Text>
        </TouchableOpacity>
      </View>
    );
  
};
const styles=StyleSheet.create({
  item: {
    flexDirection: 'row',
    backgroundColor: '#fafafa',
    padding: 20,
    marginVertical: 16,
    marginHorizontal: 16,
    borderRadius:5,
    borderWidth:1,
    width: '90%',
  },
  title: {
    flex:1,
    fontSize: 32,
  },
  description:{
    fontSize: 16,
    flex:1,
  },
  price:{
    
    fontSize: 28,
    flex:1,
  },
});
export default ProductItem;