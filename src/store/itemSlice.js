import { createSlice,createAsyncThunk} from "@reduxjs/toolkit";
export const fetchProducts=createAsyncThunk(
     'products/fetchProducts',
     async function(_,{rejectWithValue}){
         try{
             const response=await fetch ('https://bsa-marketplace.azurewebsites.net/api/Products');
                if(!response.ok){
                throw new Error('Server Error!');
                }
                const data =await response.json();
                return data;
            }catch(error){
                 return rejectWithValue(error.message);
            }
        }
);
export const fetchProductsDescr=createAsyncThunk(
    'products/fetchProductsDescr',
    async function(id,{rejectWithValue}){
        try{
            const response=await fetch (`https://bsa-marketplace.azurewebsites.net/api/Products/${id}`);
               if(!response.ok){
               throw new Error('Server Error!');
               }
               const data =await response.json();
               return data;
           }catch(error){
                return rejectWithValue(error.message);
           }
       }
);

const productsSlice=createSlice({
    name:'products',
    initialState:{
        products:[],
       
        status:null,
    },
    reducers:{
        addProducts(state){
            state.products.push({
                id: new Date().toISOString(),
                title: "jablko",
                price: 400,
                description: "jjjjabko",
                picture: null

            })
        },
        // removeProducts(state,action){
        //     state.products.push({
        //         id:"id",
        //         text: "dfdf"

        //     })
        // },
    },
    extraReducers:{
        [fetchProducts.pending]:(state)=>{
            state.status='loading';
        },
        [fetchProducts.fulfilled]:(state,action)=>{
            state.status='resolve';
            state.products=action.payload;
        },
        [fetchProducts.rejected]:(state)=>{
            state.status='rejected';
            state.error=action.payload;
        },
        
        
    }
});
export const{addProducts}=productsSlice.actions;
export default productsSlice.reducer;